package am.nac.nelsartcode.services;

import am.nac.nelsartcode.model.dto.LessonsDto;
import am.nac.nelsartcode.model.repos.courses.LessonsRepo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class LessonsJoinClass{

    @Resource
    private LessonsRepo lessonsRepo;

    public List<LessonsDto> join(){
        return lessonsRepo.join();
    }
}
