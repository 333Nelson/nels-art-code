package am.nac.nelsartcode.services;

import am.nac.nelsartcode.model.dto.Dto;

import java.util.List;

public interface JoinService {
    public List<Dto> join();
}
