package am.nac.nelsartcode.services;

import am.nac.nelsartcode.model.dto.CoursesDto;
import am.nac.nelsartcode.model.repos.courses.CoursesRepo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CoursesJoinService{

    @Resource
    private CoursesRepo coursesRepo;

    public List<CoursesDto> getCourses(){
        return coursesRepo.join();
    }
}
