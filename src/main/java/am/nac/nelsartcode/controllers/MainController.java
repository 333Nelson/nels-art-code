package am.nac.nelsartcode.controllers;

import am.nac.nelsartcode.mail.services.MailService;
import am.nac.nelsartcode.model.dto.CoursesDto;
import am.nac.nelsartcode.model.repos.users.UnAuthUsersRepo;
import am.nac.nelsartcode.services.CoursesJoinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class MainController {

    private MailService mailService;
    private CoursesJoinService coursesJoinService;
    private UnAuthUsersRepo unAuthUsersRepo;

    @Autowired
    public MainController(MailService mailService, CoursesJoinService coursesJoinService, UnAuthUsersRepo unAuthUsersRepo) {
        this.mailService = mailService;
        this.coursesJoinService = coursesJoinService;
        this.unAuthUsersRepo = unAuthUsersRepo;
    }

    @CrossOrigin()
    @GetMapping("/")
    public String index(){

        return "<h3>Connection Completed!!!</h3>";
    }

    /*@CrossOrigin
    @GetMapping("/mail")
    public String mail(){

        String mailContent = "<a href='http://localhost:3000/registration/'></a>";

        Mail mail = new Mail();
        mail.setMailFrom("community.nac@gmail.com");
        mail.setMailTo("arthurgaloyan975@gmail.com");
        mail.setMailSubject("Spring Boot - Email Example");
        mail.setMailContent(mailContent);

        mailService.sendEmail(mail);

        return "Mail Send!!!";
    }*/

    @CrossOrigin()
    @GetMapping("/courses")
    public List<CoursesDto> getCourses(){

        return coursesJoinService.getCourses();
    }

}
