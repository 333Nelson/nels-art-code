package am.nac.nelsartcode.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TestController {

    @GetMapping("/courses")
    public String add(){
        return "courses";
    }

    @GetMapping("/categories")
    public String categories(){
        return "categories";
    }

    @GetMapping("/users")
    public String users(){
        return "users";
    }
}
