package am.nac.nelsartcode.controllers;

import am.nac.nelsartcode.model.models.courses.Categories;
import am.nac.nelsartcode.model.models.courses.Courses;
import am.nac.nelsartcode.model.models.users.Users;
import am.nac.nelsartcode.model.repos.courses.CategoriesRepo;
import am.nac.nelsartcode.model.repos.courses.CoursesRepo;
import am.nac.nelsartcode.model.repos.users.UsersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestRestController {

    private CoursesRepo coursesRepo;
    private UsersRepo usersRepo;
    private CategoriesRepo categoriesRepo;

    @Autowired
    public TestRestController(CoursesRepo coursesRepo, UsersRepo usersRepo, CategoriesRepo categoriesRepo) {
        this.coursesRepo = coursesRepo;
        this.usersRepo = usersRepo;
        this.categoriesRepo = categoriesRepo;
    }

    @GetMapping("/courses/add")
    public Courses addCourse(@RequestParam String name, @RequestParam String description,
            @RequestParam String image){

        Courses courses = new Courses(name, description, image);
        courses.setCreatedAt(null);
        courses.setDeleted(false);
        coursesRepo.save(courses);

        return courses;
    }

    @GetMapping("/users/add")
    public Users addUser(@RequestParam String name, @RequestParam String email,
                     @RequestParam String phone, @RequestParam String categoryName){

        Users user = new Users(name, email, phone);
        user.setCreatedAt(null);
        usersRepo.save(user);

        Categories categories = new Categories(name);
        categories.setCreatedAt(null);
        categoriesRepo.save(categories);

        return user;
    }

}
