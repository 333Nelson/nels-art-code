package am.nac.nelsartcode.controllers;

import am.nac.nelsartcode.model.dao.users.UsersDao;
import am.nac.nelsartcode.model.dto.UsersDto;
import am.nac.nelsartcode.model.dto.reg.RegDto;
import am.nac.nelsartcode.model.repos.users.UnAuthUsersRepo;
import am.nac.nelsartcode.model.repos.users.UsersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class RegController {

    UsersDao usersDao;
    UsersRepo usersRepo;
    UnAuthUsersRepo unAuthUsersRepo;

    @Autowired
    public RegController(UsersDao usersDao, UsersRepo usersRepo, UnAuthUsersRepo unAuthUsersRepo) {
        this.usersDao = usersDao;
        this.usersRepo = usersRepo;
        this.unAuthUsersRepo = unAuthUsersRepo;
    }

    @CrossOrigin
    @PostMapping(path = "/registration", headers="Content-Type=multipart/form-data")
    public String reg(@RequestBody RegDto regDto){
        String message;

        if(usersDao.isMailExists(regDto.getEmail())){
            message = "Հաստատեք E-Mail-ը";
            usersDao.addUnAuthUser(regDto);

        }else{
            message = "E-Mail-ը արդեն օգտագործվում է";
        }

        return message;
    }


    @CrossOrigin
    @PostMapping(path = "/auth")//, consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public String auth(AuthDto auth){
        String message;

        try{
            usersDao.addAuthUser(Long.parseLong(auth.getId()), auth.getPassword());
            message = "Registration Complete";

        }catch (Exception e){
            message = "Registration Failed";
        }

        return message;
    }

    @GetMapping("/user")
    public UsersDto getUserByToken(@RequestParam(name = "token") String token){

        return unAuthUsersRepo.getByToken(token);
    }
}
