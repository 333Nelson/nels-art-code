package am.nac.nelsartcode.mail.services;

import java.io.UnsupportedEncodingException;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import am.nac.nelsartcode.mail.bean.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service("mailService")
public class MailServiceImpl implements MailService {

    @Autowired
    JavaMailSender mailSender;

    private static final String from = "community.nac@gmail.com";
    private static final String subject = "Registration Validation";

    public void sendEmail(String to, String name, String token) {

        String content = "Congratulations for Registration, Dear " + name +
                "/nClick on URL to Validate Your Email" +
                "/n<a href='http://localhost:3000/registration/" + token
                + "'>http://localhost:3000/registration/"+ token + "</a>";
        
        Mail mail = new Mail();
        mail.setMailFrom(from);
        mail.setMailTo(to);
        mail.setMailSubject(subject);
        mail.setMailContent(content);
        
        MimeMessage mimeMessage = mailSender.createMimeMessage();

        try {

            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);

            mimeMessageHelper.setSubject(mail.getMailSubject());
            mimeMessageHelper.setFrom(new InternetAddress(mail.getMailFrom(),
                    "nac.am"));
            mimeMessageHelper.setTo(mail.getMailTo());
            mimeMessageHelper.setText(mail.getMailContent());

            mailSender.send(mimeMessageHelper.getMimeMessage());

        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}
