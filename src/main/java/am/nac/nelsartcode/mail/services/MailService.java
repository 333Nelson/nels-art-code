package am.nac.nelsartcode.mail.services;


import am.nac.nelsartcode.mail.bean.Mail;

public interface MailService {
    public void sendEmail(String to, String name, String token);
}
