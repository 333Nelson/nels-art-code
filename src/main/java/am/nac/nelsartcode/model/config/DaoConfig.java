package am.nac.nelsartcode.model.config;

import am.nac.nelsartcode.model.dao.ContactUsDao;
import am.nac.nelsartcode.model.dao.CoursesDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DaoConfig {


    @Bean
    public ContactUsDao contactUsDao(){
        return new ContactUsDao();
    }

    @Bean
    public CoursesDao coursesDao(){
        return new CoursesDao();
    }
}
