package am.nac.nelsartcode.model.models.permissions;

import am.nac.nelsartcode.model.models.roles.RolePermissions;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table
@EntityListeners(AuditingEntityListener.class)
public class Permissions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @CreatedDate
    @Column(columnDefinition = "TIMESTAMP")
    private Date createdAt; //created date-time

    /*
     * RolePermissions-Permissions-Roles OneToOne Relationship by Permission ID
     * Permissions object name in RolePemissions: permission;
     */
    @OneToMany(mappedBy = "permission")
    private List<RolePermissions> rolePermissions;

    public Permissions() {
    }

    public Permissions(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
