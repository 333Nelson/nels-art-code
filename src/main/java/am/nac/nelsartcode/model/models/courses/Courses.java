package am.nac.nelsartcode.model.models.courses;

import am.nac.nelsartcode.model.models.users.Users;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
@EntityListeners(AuditingEntityListener.class)
public class Courses {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //id


    /*
     * Courses-Users OneToOne Relationship by User ID
     * Courses object name in Users: courses;
     */
    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private Users user; // registred user id


    /*
     * Courses-Categories OneToOne Relationship by Category ID
     * Courses object name in Categories: courses;
     */
    @OneToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private Categories category;

    private String name; //course name
    private String image; //course

    @Column(columnDefinition = "TEXT")
    private String description; //course description

    @Column(columnDefinition = "BOOL DEFAULT 0")
    private Boolean isDeleted; //is deleted

    @CreatedDate
    @Column(columnDefinition = "TIMESTAMP")
    private Date createdAt; //created date-time

    /*
     * Courses-Lessons OneToOne Relationship by Course ID
     * Courses object name in Lessons: course;
     *//*
    @OneToOne(mappedBy = "course")
    private Lessons lessons;*/

    public Courses() {
    }

    public Courses(String name, String image, String description) {
        this.name = name;
        this.image = image;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Categories getCategory() {
        return category;
    }

    public void setCategory(Categories category) {
        this.category = category;
    }
}
