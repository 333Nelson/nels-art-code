package am.nac.nelsartcode.model.models.users;

import am.nac.nelsartcode.model.models.courses.Categories;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Entity
public class UserCourseCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    /*
     * UserCourseCategory-Users OneToMany Relationship
     * Object name in courses: user
     */
    @ManyToOne
    @JoinColumn(name="user_id", referencedColumnName = "id")
    private Users user;

    @ManyToOne
    @JoinColumn(name="category_id", referencedColumnName = "id")
    private Categories category;

    @CreatedDate
    @Column(columnDefinition = "TIMESTAMP")
    private Date createdAt; //created date-time

    public UserCourseCategory() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Categories getCategory() {
        return category;
    }

    public void setCategory(Categories category) {
        this.category = category;
    }
}
