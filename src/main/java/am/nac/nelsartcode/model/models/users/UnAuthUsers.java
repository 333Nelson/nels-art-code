package am.nac.nelsartcode.model.models.users;

import javax.persistence.*;

@Entity
public class UnAuthUsers {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    /*
     * UnAuthUsers-Users OneToOne Relationship
     * Object name in UnAuthUsers: unAuthUsers
     */
    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private Users user;

    @Column(name = "token")
    private String token;

    public UnAuthUsers() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }
}
