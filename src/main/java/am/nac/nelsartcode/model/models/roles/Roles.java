package am.nac.nelsartcode.model.models.roles;

import am.nac.nelsartcode.model.models.users.UserRoles;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table
@EntityListeners(AuditingEntityListener.class)
public class Roles {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    /*
     * UserRoles-Users-Roles OneToMany Relationship
     * Object name in UserRoles: role
     */
    @OneToMany(mappedBy = "role")
    private List<UserRoles> userRoles;

    /*
     * RolePermissions-Permissions-Roles OneToMany Relationship by Role ID
     * Roles object name in RolePermissions: role;
     */
    @OneToMany(mappedBy = "role")
    private List<RolePermissions> rolePermissions;


    @CreatedDate
    @Column(columnDefinition = "TIMESTAMP")
    private Date createdAt; //created date-time

    public Roles() {
    }

    public Roles(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
