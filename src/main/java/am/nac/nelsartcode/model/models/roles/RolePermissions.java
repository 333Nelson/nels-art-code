package am.nac.nelsartcode.model.models.roles;

import am.nac.nelsartcode.model.models.permissions.Permissions;
import am.nac.nelsartcode.model.models.roles.Roles;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
@EntityListeners(AuditingEntityListener.class)
public class RolePermissions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    /*
     * RolePermissions-Permissions-Roles OneToMany Relationship by Role ID
     * RolePermission object name in Roles: rolePermissions;
     */
    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    private Roles role;

    /*
     * RolePermissions-Permissions-Roles OneToMany Relationship by Permission ID
     * RolePermission object name in Permissions: rolePermissions;
     */
    @ManyToOne
    @JoinColumn(name = "permission_id", referencedColumnName = "id")
    private Permissions permission;


    @CreatedDate
    @Column(columnDefinition = "TIMESTAMP")
    private Date createdAt; //created date-time

    public RolePermissions() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
