package am.nac.nelsartcode.model.models.users;

import am.nac.nelsartcode.model.models.courses.Courses;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table
@EntityListeners(AuditingEntityListener.class)
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="name", columnDefinition = "VARCHAR(30)")
    private String name;

    @Column(name="email", columnDefinition = "VARCHAR(30)")
    private String email;

    @Column(name="password", columnDefinition = "VARCHAR(30)")
    private String password;

    @Column(name="phone", columnDefinition = "VARCHAR(30)")
    private String phone;


    /*
     * Courses-Users OneToOne Relationship
     * Object name in courses: user
     */
    @OneToOne(mappedBy = "user")
    private Courses courses;

    /*
     * UnAuthUsers-Users OneToOne Relationship
     * Object name in UnAuthUsers: user
     */
    @OneToOne(mappedBy = "user")
    private UnAuthUsers unAuthUsers;

    /*
     * UserCourseCategory-Users-Categories OneToMany Relationship
     * Object name in courses: user
     */
    @OneToMany(mappedBy = "user")
    private List<UserCourseCategory> userCourseCategory;

    /*
     * UserRoles-Users-Roles OneToMany Relationship
     * Object name in UserRoles: user
     */
    @OneToMany(mappedBy = "user")
    private List<UserRoles> userRoles;


    @CreatedDate
    @Column(columnDefinition = "TIMESTAMP")
    private Date createdAt; //created date-time

    public Users() {
    }

    public Users(String name, String email, String phone) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.password = "";
    }

    public Users(String name, String email, String password, String phone) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.phone = phone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
