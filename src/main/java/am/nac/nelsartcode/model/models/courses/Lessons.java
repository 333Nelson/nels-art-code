package am.nac.nelsartcode.model.models.courses;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table
@EntityListeners(AuditingEntityListener.class)
public class Lessons {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    /*
     * Courses-Lessons OneToOne Relationship by Course ID
     * Lessons object name in Courses: lessons;
     */
    @OneToOne
    @JoinColumn(name = "course_id", referencedColumnName = "id")
    private Courses course;


    private String title;

    @Column(columnDefinition = "TEXT")
    private String description;

    private int time;
    //private int count;

    @CreatedDate
    @Column(columnDefinition = "TIMESTAMP")
    private Date createdAt; //created date-time

    public Lessons() {
    }

    public Lessons(String title, String description, int time) {
        this.title = title;
        this.description = description;
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
