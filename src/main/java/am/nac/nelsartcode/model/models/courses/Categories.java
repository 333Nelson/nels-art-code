package am.nac.nelsartcode.model.models.courses;

import am.nac.nelsartcode.model.models.users.UserCourseCategory;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table
@EntityListeners(AuditingEntityListener.class)
public class Categories {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name; //category name

    /*
     * Courses-Categories OneToOne Relationship
     * Object name in courses: category
     */
    @OneToOne(mappedBy = "category")
    private Courses courses;

    /*
     * UserCourseCategory-Categories OneToMany Relationship
     * Object name in courses: category
     */
    @OneToMany(mappedBy = "category")
    private List<UserCourseCategory> userCourseCategory;

    @CreatedDate
    @Column(columnDefinition = "TIMESTAMP")
    private Date createdAt; //created at data-time

    public Categories() {
    }

    public Categories(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
