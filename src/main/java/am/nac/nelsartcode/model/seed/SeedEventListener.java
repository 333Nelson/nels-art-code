package am.nac.nelsartcode.model.seed;

import am.nac.nelsartcode.model.models.courses.Categories;
import am.nac.nelsartcode.model.models.courses.Courses;
import am.nac.nelsartcode.model.models.users.Users;
import am.nac.nelsartcode.model.repos.courses.CategoriesRepo;
import am.nac.nelsartcode.model.repos.courses.CoursesRepo;
import am.nac.nelsartcode.model.repos.users.UsersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class SeedEventListener {

    private CategoriesRepo categoriesRepo;
    private UsersRepo usersRepo;
    private CoursesRepo coursesRepo;

    @Autowired
    public SeedEventListener(CategoriesRepo categoriesRepo, UsersRepo usersRepo,
                             CoursesRepo coursesRepo) {
        this.categoriesRepo = categoriesRepo;
        this.usersRepo = usersRepo;
        this.coursesRepo = coursesRepo;
    }

    @EventListener
    public void seed(ContextRefreshedEvent event){
        seedCategoriesTable();
        seedUsersTable();
        seedCoursesTable();
    }

    /*
     *
     * Courses Table Seeder
     * Creating Default Courses If Not Exists
     *
     */
    private void seedCoursesTable(){

        String name = "HTML & CSS Course";
        String description = "HTML & CSS Course Description";
        String image = "course.png";
        Boolean isDeleted = false;

        if(coursesRepo.findByName(name).isEmpty()){

            String categoryName = "WEB";
            Categories category = categoriesRepo.findByName(categoryName);
            String userName = "Admin Name";
            Users user = usersRepo.findByName(userName);

            Courses course = new Courses();
            course.setName(name);
            course.setDescription(description);
            course.setImage(image);
            course.setDeleted(isDeleted);
            course.setUser(user);
            course.setCategory(category);

            coursesRepo.save(course);

        }
    }

    /*
     *
     * Categories Table Seeder
     * Creating Default Categories If Not Exists
     * Creating WEB as Default Category
     *
     */
    private void seedCategoriesTable(){

        String name = "WEB";

        if(categoriesRepo.findByName(name) == null){

            Categories category = new Categories();
            category.setName(name);

            categoriesRepo.save(category);
        }
    }

    /*
     *
     * Users Table Seeder
     * Creating Default Users If Not Exists
     * Creating ADMIN & USER Users as Default Category
     *
     */
    private void seedUsersTable(){

        String name = "Admin Name";
        String email = "admin@email.com";
        String password = "password";
        String phone = "9999999999";

        if(usersRepo.findByName(name) == null) {

            Users user = new Users();

            user.setName(name);
            user.setEmail(email);
            user.setPassword(password);
            user.setPhone(phone);

            usersRepo.save(user);
        }
    }
}
