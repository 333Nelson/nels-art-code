package am.nac.nelsartcode.model.dao.users;

import am.nac.nelsartcode.mail.services.MailService;
import am.nac.nelsartcode.model.dto.reg.RegDto;
import am.nac.nelsartcode.model.models.users.Users;
import am.nac.nelsartcode.model.repos.courses.CategoriesRepo;
import am.nac.nelsartcode.model.repos.users.UnAuthUsersRepo;
import am.nac.nelsartcode.model.repos.users.UserCourseCategoryRepo;
import am.nac.nelsartcode.model.repos.users.UsersRepo;
import am.nac.nelsartcode.token.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UsersDao {

    private UsersRepo usersRepo;
    private UserCourseCategoryRepo userCourseCategoryRepo;
    private UnAuthUsersRepo unAuthUsersRepo;
    private CategoriesRepo categoriesRepo;
    private MailService mailService;

    @Autowired
    public UsersDao(UsersRepo usersRepo, UserCourseCategoryRepo userCourseCategoryRepo,
                    UnAuthUsersRepo unAuthUsersRepo, CategoriesRepo categoriesRepo,
                    MailService mailService) {
        this.usersRepo = usersRepo;
        this.userCourseCategoryRepo = userCourseCategoryRepo;
        this.unAuthUsersRepo = unAuthUsersRepo;
        this.categoriesRepo = categoriesRepo;
        this.mailService = mailService;
    }


    /*
     * add to Users
     * add to UnAuthUsers user id and token
     * add to UserCoursesCategory
     */
    public void addUnAuthUser(RegDto regDto){

        // Creating new Users row
        Users users = new Users(regDto.getName(), regDto.getEmail(), regDto.getPhone());
        usersRepo.save(users);

        // Creating UserCourseCategory row
        UserCourseCategoryDao userCourseCategoryDao = new UserCourseCategoryDao();
        userCourseCategoryRepo.save(userCourseCategoryDao.getUserCourseCategory(users,
                categoriesRepo.findById(Long.parseLong(regDto.getCategory())).get()));

        // Creating UnAuthUsers row
        Token token = new Token();
        String genToken = token.generateNewToken();

        UnAuthUsersDao unAuthUsersDao = new UnAuthUsersDao();
        unAuthUsersRepo.save(unAuthUsersDao.getUnAuthUser(users,
                genToken));

        mailService.sendEmail(regDto.getEmail(), regDto.getName(), genToken);
    }

    // Add password to user by id
    public void addAuthUser(Long id, String password){
        Users user = usersRepo.findById(id).get();

        user.setPassword(password);

        unAuthUsersRepo.deleteById(id);
    }

    // Checking is exist email
    public boolean isMailExists(String email){
        boolean isExists;

        return usersRepo.findByEmail(email).isEmpty();
    }


}
