package am.nac.nelsartcode.model.dao;

import am.nac.nelsartcode.model.models.ContactUs;
import am.nac.nelsartcode.model.repos.ContactUsRepo;
import org.springframework.beans.factory.annotation.Autowired;

public class ContactUsDao {

    private ContactUsRepo contactUsRepo;

    public ContactUsDao() {
    }

    @Autowired
    public ContactUsDao(ContactUsRepo contactUsRepo) {
    }

    public void addMessage(ContactUs contactUs){
        contactUsRepo.save(contactUs);
    }

}
