package am.nac.nelsartcode.model.dao.users;

import am.nac.nelsartcode.model.models.courses.Categories;
import am.nac.nelsartcode.model.models.users.UserCourseCategory;
import am.nac.nelsartcode.model.models.users.Users;

@SuppressWarnings("ALL")
public class UserCourseCategoryDao {

    public UserCourseCategory getUserCourseCategory(Users user, Categories category){

        UserCourseCategory userCourseCategory = new UserCourseCategory();
        userCourseCategory.setUser(user);
        userCourseCategory.setCategory(category);

        return userCourseCategory;
    }
}
