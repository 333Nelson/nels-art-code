package am.nac.nelsartcode.model.dao;

import am.nac.nelsartcode.model.models.courses.Categories;
import am.nac.nelsartcode.model.models.courses.Courses;
import am.nac.nelsartcode.model.models.users.Users;
import am.nac.nelsartcode.model.repos.courses.CoursesRepo;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class CoursesDao {

    private CoursesRepo coursesRepo;

    public CoursesDao() {
    }

    @Autowired
    public CoursesDao(CoursesRepo coursesRepo) {
    }

    public void add(Courses course){

        coursesRepo.save(course);
    }

    public Optional<Courses> findById(Long id){
        return coursesRepo.findById(id);
    }

    public void delete(Courses course){
        course.setDeleted(true);
    }
}
