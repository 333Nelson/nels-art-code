package am.nac.nelsartcode.model.dao.users;

import am.nac.nelsartcode.model.models.courses.Categories;
import am.nac.nelsartcode.model.models.users.UnAuthUsers;
import am.nac.nelsartcode.model.models.users.UserCourseCategory;
import am.nac.nelsartcode.model.models.users.Users;

public class UnAuthUsersDao {

    public UnAuthUsers getUnAuthUser(Users user, String token){

        UnAuthUsers unAuthUsers = new UnAuthUsers();
        unAuthUsers.setUser(user);
        unAuthUsers.setToken(token);

        return unAuthUsers;
    }
}
