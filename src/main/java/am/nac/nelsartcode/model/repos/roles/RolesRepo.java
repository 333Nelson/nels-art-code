package am.nac.nelsartcode.model.repos.roles;

import am.nac.nelsartcode.model.models.roles.Roles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolesRepo extends JpaRepository<Roles, Long> {
}
