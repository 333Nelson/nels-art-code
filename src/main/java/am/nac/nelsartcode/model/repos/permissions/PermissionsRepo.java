package am.nac.nelsartcode.model.repos.permissions;

import am.nac.nelsartcode.model.models.permissions.Permissions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionsRepo extends JpaRepository<Permissions, Long> {
}
