package am.nac.nelsartcode.model.repos.permissions;

import am.nac.nelsartcode.model.models.roles.RolePermissions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolePermissionsRepo extends JpaRepository<RolePermissions, Long> {
}
