package am.nac.nelsartcode.model.repos.users;

import am.nac.nelsartcode.model.models.users.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepo extends JpaRepository<Users, Long> {

    Users findByName(String name);
    List<Users> findByEmail(String email);

}
