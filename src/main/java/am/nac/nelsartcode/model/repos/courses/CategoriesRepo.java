package am.nac.nelsartcode.model.repos.courses;

import am.nac.nelsartcode.model.models.courses.Categories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoriesRepo extends JpaRepository<Categories, Long> {
    Categories findByName(String name);
}
