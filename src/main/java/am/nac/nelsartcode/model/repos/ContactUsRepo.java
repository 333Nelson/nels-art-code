package am.nac.nelsartcode.model.repos;

import am.nac.nelsartcode.model.models.ContactUs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactUsRepo extends JpaRepository<ContactUs, Long> {
}
