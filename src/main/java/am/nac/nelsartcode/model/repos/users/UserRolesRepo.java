package am.nac.nelsartcode.model.repos.users;

import am.nac.nelsartcode.model.models.users.UserRoles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRolesRepo extends JpaRepository<UserRoles, Long> {
}
