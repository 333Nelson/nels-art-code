package am.nac.nelsartcode.model.repos.courses;

import am.nac.nelsartcode.model.dto.CoursesDto;
import am.nac.nelsartcode.model.models.courses.Courses;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CoursesRepo extends JpaRepository<Courses, Long> {
        Iterable<Courses> findByIsDeleted(Boolean isDeleted);
        List<Courses> findByName(String name);

        @Query("SELECT new am.nac.nelsartcode.model.dto.CoursesDto(c.name, cat.name, u.name, c.description, c.image) " +
                "FROM Courses c INNER JOIN c.category cat INNER JOIN c.user u")
        List<CoursesDto> join();
}
