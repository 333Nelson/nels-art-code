package am.nac.nelsartcode.model.repos.courses;

import am.nac.nelsartcode.model.dto.LessonsDto;
import am.nac.nelsartcode.model.models.courses.Lessons;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LessonsRepo extends JpaRepository<Lessons, Long> {

    @Query(value = "SELECT new LessonsDto(l.title, l.description, l.time, c.name) " +
            "FROM Lessons l INNER JOIN l.course c", nativeQuery = true)
    List<LessonsDto> join();
}
