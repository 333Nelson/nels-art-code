package am.nac.nelsartcode.model.repos.users;

import am.nac.nelsartcode.model.dto.UsersDto;
import am.nac.nelsartcode.model.models.users.UnAuthUsers;
import am.nac.nelsartcode.model.models.users.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UnAuthUsersRepo extends JpaRepository<UnAuthUsers, Long> {

    @Query("SELECT new am.nac.nelsartcode.model.dto.UsersDto(u.id, u.name, u.email, u.phone) FROM UnAuthUsers uau" +
            " INNER JOIN uau.user u WHERE uau.token=:token")
    UsersDto getByToken(String token);
}
