package am.nac.nelsartcode.model.dto;

public class LessonsDto{

    private String title;
    private String description;
    private String time;
    private String courseName;

    public LessonsDto(String title, String description, String time, String courseName) {
        this.title = title;
        this.description = description;
        this.time = time;
        this.courseName = courseName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }
}
