/*
 * Courses, Categories and Users Joined Tables DTO
 */
package am.nac.nelsartcode.model.dto;

public class CoursesDto{

    private String name;
    private String category;
    private String user;
    private String description;
    private String image;

    public CoursesDto(String name, String category, String user, String description, String image) {
        this.name = name;
        this.category = category;
        this.user = user;
        this.description = description;
        this.image = image;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
