package am.nac.nelsartcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NelsArtCodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(NelsArtCodeApplication.class, args);
    }

}
